<?php

namespace Drupal\render_menu\TwigExtension;

/**
 * Class RenderMenuExtension.
 *
 * @package Drupal\render_menu
 */
class RenderMenuExtension extends \Twig_Extension {

    /**
     * {@inheritdoc}
     */
    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('render_menu',
                [$this, 'render_menu'],
                ['is_safe' => ['html']]
            ),
        ];
    }

    /**
     * The php function to Render a menu
     * @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Menu!menu.api.php/group/menu/8.2.x#sec_rendering
     */
    public function render_menu($menu_name) {
        $menu_tree = \Drupal::menuTree();

        // Build the typical default set of menu tree parameters.
        $parameters = $menu_tree->getCurrentRouteMenuTreeParameters($menu_name);

        // Load the tree based on this set of parameters.
        $tree = $menu_tree->load($menu_name, $parameters);

        // Transform the tree using the manipulators you want.
        $manipulators = array(
            // Only show links that are accessible for the current user.
            array('callable' => 'menu.default_tree_manipulators:checkAccess'),
            // Use the default sorting of menu links.
            array('callable' => 'menu.default_tree_manipulators:generateIndexAndSort'),
        );
        $tree = $menu_tree->transform($tree, $manipulators);

        // Finally, build a renderable array from the transformed tree.
        $menu = $menu_tree->build($tree);

        return array('#markup' => \Drupal::service('renderer')->render($menu));
    }

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return 'render_menu';
    }

}
